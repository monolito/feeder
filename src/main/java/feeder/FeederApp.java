package feeder;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Request;
import org.asynchttpclient.Response;
import org.kohsuke.args4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.asynchttpclient.Dsl.*;
import static org.kohsuke.args4j.OptionHandlerFilter.ALL;

/**
 *
 */
public class FeederApp {
    @Option(name = "-i", usage = "input folder", required = true)
    private File input = new File(".");

    @Option(name = "-maxParallelism", usage = "maximum number of concurrent connections")
    private int maxParallelism = 10;

    @Option(name = "-apiUri", usage = "api url")
    private String apiUri = "http://localhost:5000/api/named_entities";

    private AsyncHttpClient asyncHttpClient;

    void init(String[] args) throws CmdLineException {
        CmdLineParser parser = new CmdLineParser(this, ParserProperties.defaults().withUsageWidth(80));

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java FeederApp [options...] arguments...");
            parser.printUsage(System.err);
            System.err.println();
            System.err.println("  Example: java FeederApp" + parser.printExample(ALL));
            throw e;
        }

        asyncHttpClient = asyncHttpClient(config().setMaxConnections(maxParallelism));
    }

    private Stream<Path> getFiles() {
        try {
            return Files.walk(this.input.toPath())
                    .filter(Files::isRegularFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Stream.empty();
    }

    private void shutdown(ExecutorService executor, ScheduledExecutorService retryExecutor) {
        try {
            asyncHttpClient.close();
        } catch (IOException e) {
            logger.error("Ignoring.", e);
        }

        executor.shutdownNow();
        retryExecutor.shutdownNow();
        System.exit(1);
    }

    private void sendFile(File file, int retries, ConcurrentLinkedQueue<Path> queue, ExecutorService executor,
                          ScheduledExecutorService retryExecutor, ConcurrentHashMap<Path, Response> responses, CompletableFuture<Object> completableFuture) {
        logger.info("Sending file {}", file.getAbsolutePath());
        Request request = post(this.apiUri).setBody(file).build();
        ListenableFuture<Response> responseFuture = asyncHttpClient.executeRequest(request);
        responseFuture.addListener(() -> {
            try {
                Response response = responseFuture.get();
                responses.put(file.toPath(), response);

                if (response.getStatusCode() == 503) {
                    if (retries > 2) shutdown(executor, retryExecutor); // NOTICE: Server is busy. Giving up.

                    retryExecutor.schedule(() ->
                            sendFile(file, retries + 1, queue, executor, retryExecutor, responses, completableFuture), 10L * retries, TimeUnit.SECONDS);
                } else {
                    Optional.ofNullable(queue.poll()).ifPresentOrElse(path -> sendFile(path.toFile(), 0, queue, executor, retryExecutor, responses, completableFuture),
                            () -> completableFuture.complete(null));
                }
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Error on completed request.", e);
            }
        }, executor).toCompletableFuture();
    }

    public void run() {
        ConcurrentHashMap<Path, Response> responses = new ConcurrentHashMap<>();
        ScheduledExecutorService retryExecutor = new ScheduledThreadPoolExecutor(4);
        ExecutorService executor = new ThreadPoolExecutor(4, 20, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<>());
        ConcurrentLinkedQueue<Path> pathsQueue = getFiles().collect(Collectors.toCollection(ConcurrentLinkedQueue::new));

        Stream<CompletableFuture<Object>> futureStream = IntStream.range(0, maxParallelism).mapToObj(i ->
                Optional.ofNullable(pathsQueue.poll()).map(path -> {
                    CompletableFuture<Object> completableFuture = new CompletableFuture<>();
                    sendFile(path.toFile(), 0, pathsQueue, executor, retryExecutor, responses, completableFuture);
                    return completableFuture;
                }).orElseGet(() -> CompletableFuture.completedFuture(null)));

        futureStream.forEach(future -> {
            try {
                future.get(); // blocks until futures are completed, i.e. all requests completed.
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        executor.shutdown();
        retryExecutor.shutdownNow();
        try {
            asyncHttpClient.close();
        } catch (IOException e) {
            logger.error("Ignoring", e);
        }
        responses.forEach((path, response) -> System.out.println(String.format("%s %s", path, response.getResponseBody())));

        logger.info("Done.");
    }

    private static final Logger logger = LoggerFactory.getLogger(FeederApp.class);

    public static void main(String[] args) throws CmdLineException {
        FeederApp app = new FeederApp();
        app.init(args);
        app.run();
    }
}
