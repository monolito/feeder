# feeder

Client application for the **seshat** server.

## Description

Uploads text documents from the input folder recursively to the seshat server. Calls the _named_entities_ API
concurrently. 

## Design and Considerations

- Using an async non-blocking http client implementation for better performance;
- Handle back-pressure with exponential back-off (503 http status code);
- Configurable number of parallel requests (limited to avoid DOS problem).

## Installation

Requirements:
   - JRE >= 1.8

## Dev

```
./gradlew build
```


## Usage

Build or download distribution tar file from https://bitbucket.org/monolito/feeder/downloads/.

```
tar xvf feeder.tar
cd feeder
./bin/feeder -h
```
